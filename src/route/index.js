import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Login, PostScreen, Splash, DataScreen, Daftar, Profile} from '../pages';
import {StyleSheet} from 'react-native';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const GlobalScreen = () => {
  return (
    <Tab.Navigator style={style.tab}>
      <Tab.Screen name="Data" component={DataScreen} />
      <Tab.Screen name="Post" component={PostScreen} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
};

const Route = () => {
  return (
    <Stack.Navigator headerMode="none" initialRouteName="Splash">
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
        header
      />
      <Stack.Screen
        name="DaftarScreen"
        component={Daftar}
        options={{headerShown: false}}
      />
      <Stack.Screen name="DataScreen" component={DataScreen} />
      <Stack.Screen
        name="PostScreen"
        component={PostScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Global"
        component={GlobalScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Route;

const style = StyleSheet.create({
  tab: {position: 'absolute'},
});
