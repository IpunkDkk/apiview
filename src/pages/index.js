import LoginScreen from './LoginScreen';
import PostScreen from './PostScreen';
import DataScreen from './DataScreen';
import Splash from './Splash';
import DaftarScreen from './DaftarScreen';
import ProfileScreen from './ProfileScreen';
import Auth from './Auth';

export {
  LoginScreen as Login,
  DaftarScreen as Daftar,
  PostScreen as PostScreen,
  ProfileScreen as Profile,
  Auth,
  DataScreen,
  Splash,
};
