import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, TextInput, Button, Input} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Api from '../../config';

const DaftarScreen = ({navigation}) => {
  // useEffect(()=>{
  //     daftar();
  // })
  const [nama, setnama] = useState('');
  const [nik, setnik] = useState('');
  const [nokk, setnokk] = useState('');

  const daftar = () => {
    const data = {
      nama,
      nik,
      nokk,
    };
    console.log('data', data);
    Api.post('tambah', data).then(res => {
      console.log(res.data);
    });
  };

  return (
    <View style={style.container}>
      <View>
        <Text style={style.textTitle}>Tambah Data</Text>
        <TextInput
          placeholder="nama"
          style={style.input}
          value={nama}
          onChangeText={value => setnama(value)}
        />
        <TextInput
          placeholder="nik"
          keyboardType="number-pad"
          style={style.input}
          value={nik}
          onChangeText={value => setnik(value)}
        />
        <TextInput
          placeholder="nokk"
          keyboardType="number-pad"
          style={style.input}
          value={nokk}
          onChangeText={value => setnokk(value)}
        />
        <Button title="Tambah" onPress={daftar} />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    padding: 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {fontSize: 20, marginBottom: 20, textAlign: 'center'},
  title: {textAlign: 'center'},
  input: {
    height: 50,
    width: 350,
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 25,
    paddingHorizontal: 18,
  },
});

export default DaftarScreen;
