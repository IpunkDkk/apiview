import React, {useState} from 'react';
import {View, Text, StyleSheet, TextInput, Button, Input} from 'react-native';

import Api from '../../config';

const DaftarScreen = ({navigation}) => {
  const [username, setusername] = useState('');
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');

  const daftar = () => {
    const data = {
      username,
      email,
      password,
    };
    console.log('data', data);
    Api.post('register', data).then(res => {
      console.log(res.data.token);
    });
  };

  return (
    <View style={style.container}>
      <View>
        <Text style={style.textTitle}>Masukan Username Dan Password Anda!</Text>
        <TextInput
          placeholder="username"
          style={style.input}
          value={username}
          onChangeText={value => setusername(value)}
        />
        <TextInput
          placeholder="email"
          style={style.input}
          value={email}
          onChangeText={value => setemail(value)}
        />
        <TextInput
          placeholder="password"
          style={style.input}
          value={password}
          onChangeText={value => setpassword(value)}
        />
        <Button title="daftar" onPress={daftar} />
      </View>
      <View style={{padding: 20}}>
        <Button title="Login" onPress={() => navigation.navigate('Login')} />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    padding: 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {fontSize: 20, marginBottom: 20, textAlign: 'center'},
  title: {textAlign: 'center'},
  input: {
    height: 50,
    width: 350,
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 25,
    paddingHorizontal: 18,
  },
});

export default DaftarScreen;
