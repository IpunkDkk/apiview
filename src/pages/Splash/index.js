import React, {useEffect} from 'react';
import {View, Image} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Splash = ({navigation}) => {
  useEffect(() => {
    const _Validasi = async () => {
      const isLogin = await AsyncStorage.getItem('session_id');
      if (isLogin) {
        navigation.navigate('Global');
      } else {
        navigation.navigate('Login');
      }
    };
    setTimeout(() => {
      _Validasi();
    }, 2000);
  }, []);

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Image
        style={{width: 170, height: 170}}
        source={require('../../assets/splash.png')}
      />
    </View>
  );
};

export default Splash;
