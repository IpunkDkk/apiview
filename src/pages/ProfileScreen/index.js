import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';

const ProfileScreen = ({navigation}) => {
  const Keluar = async () => {
    await AsyncStorage.clear();
    navigation.navigate('Login');
  };
  return (
    <View style={style.container}>
      <Text style={style.text}>Silahkan Keluar</Text>
      <Button title="Keluar" onPress={Keluar} />
    </View>
  );
};

export default ProfileScreen;

const style = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {padding: 20, fontSize: 18},
});
