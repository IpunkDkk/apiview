import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TextInput, Button} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Api from '../../config';

const LoginScreen = ({navigation}) => {
  const [username, setusername] = useState('');
  const [password, setpassword] = useState('');

  const submit = () => {
    const data = {
      username,
      password,
    };
    // console.log("data" , data);
    Api.post('login', data)
      .then(res => {
        console.log(res.data);
        if (res.data.token) {
          AsyncStorage.setItem('session_id', res.data.token);
          navigation.navigate('Global');
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <View style={style.container}>
      <View>
        <Text style={style.title}>Silahkan Login</Text>
        <Text style={style.textTitle}>Masukan Username Dan Password Anda!</Text>
        <TextInput
          placeholder="username"
          style={style.input}
          value={username}
          onChangeText={value => setusername(value)}
        />
        <TextInput
          placeholder="password"
          style={style.input}
          value={password}
          onChangeText={value => setpassword(value)}
        />
        <Button title="Login" onPress={submit} />
      </View>
      <View style={{padding: 20}}>
        <Button
          title="Daftar"
          onPress={() => navigation.navigate('DaftarScreen')}
        />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    padding: 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {fontSize: 20, marginBottom: 20, textAlign: 'center'},
  title: {textAlign: 'center', fontSize: 20, marginBottom: 20},
  input: {
    height: 50,
    width: 350,
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 25,
    paddingHorizontal: 18,
  },
});

export default LoginScreen;
