import React, {Component} from 'react';
import {Text, View, StyleSheet, ScrollView} from 'react-native';
import Api from '../../config';

export default class DataScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  componentDidMount() {
    this.getData();
  }
  getData = () => {
    Api.get('data').then(result => {
      this.setState({
        data: result.data,
      });
    });
  };

  render() {
    const {data} = this.state;
    return (
      <ScrollView>
        <View style={style.wrapper}>
          {data.map((item, key) => {
            return (
              <View key={key}>
                <View style={style.container}>
                  <View style={style.font}>
                    <Text style={style.text}>NAMA {item.nama}</Text>
                    <Text style={style.text}>NIK {item.nik}</Text>
                    <Text style={style.text}>NOKK {item.nokk}</Text>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
      </ScrollView>
    );
  }
}

const style = StyleSheet.create({
  wrapper: {flex: 1},
  font: {},
  text: {fontSize: 12},
  container: {
    flex: 1,
    flexDirection: 'row',
    padding: 20,
    backgroundColor: 'white',
    borderRadius: 5,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
});

// export default DataScreen
